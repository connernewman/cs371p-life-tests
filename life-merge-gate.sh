#!/bin/bash
set -e

chmod +x ./RunLifeCell
chmod +x ./RunLifeConway
chmod +x ./RunLifeFredkin

# BRANCH_NAME=$(git rev-parse --abbrev-ref HEAD)
REMOTE_URL=$(git config --get remote.origin.url)
echo "REMOTE_URL: $REMOTE_URL"
REPO_NAME=${REMOTE_URL:56}

GITLAB_ID=${REPO_NAME:0:$(( $(expr index "$REPO_NAME" "/") - 1 ))}
echo "Gitlab ID: $GITLAB_ID"

echo "Checking changes..."
git fetch https://gitlab.com/gpdowning/cs371p-life-tests.git master
git branch class FETCH_HEAD
MERGE_CHANGES_FULL=$(git diff --shortstat class...)
MERGE_CHANGES=${MERGE_CHANGES_FULL:0:16}
if [[ "$MERGE_CHANGES" != " 6 files changed" ]];
then
    echo "FAIL: Commit contains extraneous changes: $MERGE_CHANGES"
    exit 1
fi

SUFFIX_LIST="RunLifeCell RunLifeConway RunLifeFredkin"
for SUFFIX in $SUFFIX_LIST; do
    RIN="${GITLAB_ID}-${SUFFIX}.in"
    ROUT="${GITLAB_ID}-${SUFFIX}.out"
    TOUT="${GITLAB_ID}-${SUFFIX}.tmp"

    echo "Checking file names..."
    if [[ ! -e "$RIN" || ! -e "$ROUT" ]]
    then
        echo "FAIL: File names incorrect (could not find $RIN and $ROUT)"
        exit 1
    fi

    echo "Checking line count..."
    COUNT=$(wc -l < "$RIN")
    echo "Line Count: $COUNT"
    if (( "$COUNT" < 100 || "$COUNT" > 100000 ));
    then
        echo "FAIL: Acceptance test line count not within [100, 100000]"
        exit 1
    fi

    echo "Checking test count..."
    FIRSTLINE=$(head -1 "$RIN")
    echo "Test Count: $FIRSTLINE"
    if (( "$FIRSTLINE" < 10 || "$FIRSTLINE" > 20 ))
    then
        echo "FAIL: Acceptance test quantity not within [10, 20]"
        exit 1
    fi

    echo "Running tests, please wait..."
    "./$SUFFIX" < "$RIN" > "$TOUT"
    echo "Checking diff..."

    DIFF=$(diff "$ROUT" "$TOUT" 2>&1 | cat -vet || true)
    # rm "$TOUT"
    if [[ "$DIFF" != "" ]]
    then
        echo "$DIFF"
        echo "FAIL: Reference solution output did not match expected output for $SUFFIX"
        echo "Learn how to read diffs: https://unix.stackexchange.com/questions/81998/understanding-of-diff-output"
        exit 1
    fi
done

echo "PASS"
exit 0
